#!/bin/bash
# 
# wp-update.sh (version:0.1)
# update & partial wordpress backup
#  Copyright (C) 2011  Charel Buchler <charel@charelbuchler.com>

# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#config vars
BLOGUPDATEURL="http://www.wordpress.org/latest.zip"
ADMINFOLDER=$SITE"/wp-admin"
INCLUDESFOLDER=$SITE"/wp-includes"
CONTENTFOLDER=$SITE"/wp-content"
CONFIGFILE=$SITE"/wp-config.php"
SQLFILEBACKUP="wordpress-dump.sql"
GETCURRDATE="date +%d-%m-%Y"
WPBACKUPDIR=$HOME"/wp-backup"

#user input
echo "Enter SITE (fullpath)"
read SITE
echo "Enter mysqluser (for dump)"
read MYSQLUSER
echo "Enter mysqlpassword"
read MYSQLPASSWORD
echo "Enter mysql-database"
read MYSQLDATABASE

#database-backup 
mysqldump -u $MYSQLUSER -p $MYSQLPASSWORD $MYSQLDATABASE > $SQLFILEBACKUP

#downloading the latest wordpress version
wget $BLOGUPDATEURL -C $SITE 

#unarchiving the wordpress
unzip $SITE"/latest.zip"

#creating wordpress-backup file
tar -czf wp-backup-$GETCURRDATE.tar.gz $ADMINFOLDER $INCLUDESFOLDER $CONTENTFOLDER $CONFIGFILE $SQLFILEBACKUP 

# Removing old files
rm -R $ADMINFOLDER
rm -R $INCLUDEDFOLDER

#Create WP-Backup Dir
mkdir -p $WPBACKUPDIR

#Moving Backupfile to wp-backupdir
mv -f wp-backup-$GETCURRDATE.tar.gz $WPBACKUPDIR

#copying new wordpress files to wp
cp -R $SITE"/wordpress/wp-admin" ../
cp -R $SITE"/wordpress/wp-includes" ../

#removing new wordpress
rm -R $SITE"/wordpress"

#removing archive
rm latest.zip

#== EOF == 
